/* -------------------------------------------------------------------------------
 LedCube.h
 
 This class is used to interface the Arduino (and any other machine via RS232 with 
 the LedCube built based on instructions found on Instructables.com (see 
 http://www.instructables.com/id/Led-Cube-8x8x8/?ALLSTEPS)
 
 NOTES:
 - This library uses the threading library ProtoThreads to run two main loops at 
    different frequencies (the layer update thread at 1000 Hz and the pattern 
    update thread at a user defined frequency)
 
 EXAMPLES:

 Initially created Daniel Pickem on 14/12/2013

 This library is in part based on the library available at 
    http://www.instructables.com/id/Led-Cube-8x8x8/?ALLSTEPS
 ------------------------------------------------------------------------------- */

#ifndef _LEDCUBE_h_
#define _LEDCUBE_h_

//------------------------------------------------------------------------------
// Includes
//------------------------------------------------------------------------------
#include <Arduino.h>
#include <pt.h>
#include "Effect.h"

/* Forward declaration of effect class, required for having a pointer or an
 * instance of the class Effect in the class LedCube 
 * See:
 * http://stackoverflow.com/questions/8526819/c-header-files-including-each-other-mutually
 */
class Effect;

//------------------------------------------------------------------------------
// Pin Definitions 
//------------------------------------------------------------------------------
/* Flip flop databus and output enable pin */
#define OE      2
#define DATA_0  3
#define DATA_1  4
#define DATA_2  5
#define DATA_3  6
#define DATA_4  7
#define DATA_5  8
#define DATA_6  9
#define DATA_7  10

/* Flip flop demultiplexer pins */
#define FF_1    11
#define FF_2    12
#define FF_3    13

/* Layer demultiplexer pins */
#define L_1     A0
#define L_2     A1
#define L_3     A2

/* Define coordinate limits */
#define MIN_X   0
#define MAX_X   7
#define MIN_Y   0
#define MAX_Y   7
#define MIN_Z   0
#define MAX_Z   7

class LedCube {
    public:
      // Constructors
      LedCube();
      
      // Destructor
      ~LedCube();
      
      // Copy constructor
      LedCube(const LedCube& srcObj);
      
      // Assignment operator
      const LedCube& operator=(const LedCube& rhsObj);

      //--------------------------------------------------------------------------
      // Public Member Functions
      //--------------------------------------------------------------------------
      void initialize();
      void run();

      static int updateLayerThread(struct pt *pt, int interval, LedCube *instance);
      static int updatePatternThread(struct pt *pt, int interval, LedCube *instance);

      /* Update pattern */
      void updatePattern();

      /* Update current layer */
      void updateLayer();

      /* Update data in flip flops */
      void updateFlipFlop(int i);
      void updateFlipFlops();

      /* Write data */
      void writeData(unsigned char data);

      /* Control demultiplexers for flip flops and layers */
      void setLayer();
      void setLayer(int output);
      void incrementLayer();
      void setFlipFlop(int output);
      void setDemultiplexer(int output, int A, int B, int C);

      /* Drawing functions */
      void setVoxel(int x, int y, int z);
      void clearVoxel(int x, int y, int z);
      bool getVoxel(int x, int y, int z);
      void alterVoxel(int x, int y, int z, bool state);
      void flipVoxel(int x, int y, int z);
      void fill(unsigned char pattern);

      /* Drawing box functions */
      void boxFilled(int x1, int y1, int z1, int x2, int y2, int z2);
      void boxWalls(int x1, int y1, int z1, int x2, int y2, int z2);
      void boxWireframe(int x1, int y1, int z1, int x2, int y2, int z2);

      /* Drawing mirror functions */
      void mirrorX();
      void mirrorY();
      void mirrorZ();

      /* Drawing shift functions */
      void shift(int axis, bool direction);   /* direction = 0 means down, 1 means up */
      void shiftX(bool direction);
      void shiftY(bool direction);
      void shiftZ(bool direction);

      /* Drawing line functions */
      void line(int x1, int y1, int z1, int x2, int y2, int z2);

      /* Set, clear, and copy planes */
      void setPlane(int i, int plane = AXIS_X, bool data = false);
      void setPlaneX(int x, bool data = false);
      void setPlaneY(int y, bool data = false);
      void setPlaneZ(int z, bool data = false);
      void copyPlane(int src, int dst, int plane = AXIS_X);
      void copyPlaneX(int src, int dst);
      void copyPlaneY(int src, int dst);
      void copyPlaneZ(int src, int dst);

      /* Check functions */
      bool inRange(int x, int y, int z);
      bool inRangeX(int x);
      bool inRangeY(int y);
      bool inRangeZ(int z);

      /* Utility functions */
      char byteLine (int start, int end);
      char flipByte(char byte);
      void argOrder(int ix1, int ix2, int *ox1, int *ox2);

      /* Get functions */
      int getRefreshRatePattern();
      int getRefreshRateLayer();
      Effect* getCurrentEffect();

      /* Set functiosn */
      void setRefreshRatePattern(int refreshRate);
      void setRefreshRateLayer(int refreshRate);
      void setEffect(String name);

    private:
      //--------------------------------------------------------------------------
      // Lifecycle
      //--------------------------------------------------------------------------
      void copyHelper(const LedCube& srcObj);
      
      //--------------------------------------------------------------------------
      // Private Member Functions
      //--------------------------------------------------------------------------
      
      //--------------------------------------------------------------------------
      // Private Member Variables
      //--------------------------------------------------------------------------
      bool init_;                               /* flag to store weather initialize function has been called */
      unsigned char cube_[MAX_Z+1][MAX_Y+1];    /* Data array holding 512 bits (one per LED) */

      int refreshRatePattern_;                  /* Specifies how often flip flop data should be updated */
      int refreshRateLayer_;                    /* Specifies how often layers are switched */
      int currentLayer_;

      static struct pt ptLayer_, ptPattern_;    /* proto threads for layer and pattern update */
      Effect* currentEffect_;                   /* store pointer to current effect */
};
#endif
