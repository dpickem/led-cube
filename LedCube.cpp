/* -------------------------------------------------------------------------------
 LedCube.cpp
 
 This class is used to interface the Arduino (and any other machine via RS232 with 
 the LedCube built based on instructions found on Instructables.com (see 
 http://www.instructables.com/id/Led-Cube-8x8x8/?ALLSTEPS)
 
 NOTES:
 
 EXAMPLES:

 Initially created Daniel Pickem on 14/12/2013
 ------------------------------------------------------------------------------- */

//------------------------------------------------------------------------------
// Includes
//------------------------------------------------------------------------------
#include <LedCube.h>

//------------------------------------------------------------------------------
// Lifecycle
//------------------------------------------------------------------------------
/* Constructors */
LedCube::LedCube() {
  /* Nothing to be done here, everything is done in initializer function */
  init_ = false;
}

/* Destructor */
LedCube::~LedCube() {
  /* Nothing to be done in destructor since LedCube class uses only static
   * memorey and no dynamically allocated memory */
}

/* Copy constructor */
LedCube::LedCube(const LedCube& srcObj) {
  copyHelper(srcObj);
}

/* Assignment operator */
const LedCube& LedCube::operator=(const LedCube& rhsObj) {
  /* Self-assignment check */
  if (this == &rhsObj) {
      return (*this);
  }
  
  /* Free old memory */
  
  /* Copy new memory */
  copyHelper(rhsObj);
  
  return (*this);
}

/* Copy helper function */
void LedCube::copyHelper(const LedCube& srcObj) {
  /* Copy cube array contents */
  memcpy(cube_, srcObj.cube_, 64);    // TODO: is this the correct direction (src to cube_) ???

  /* Copy private member variables */
  refreshRateLayer_   = srcObj.refreshRateLayer_;
  refreshRatePattern_ = srcObj.refreshRatePattern_;
  currentLayer_       = srcObj.currentLayer_; 

  /* Copy protothread structures */
  ptLayer_            = srcObj.ptLayer_;      // TODO: test if this works
  ptPattern_          = srcObj.ptPattern_;

  /* Copy effect pointer */
  currentEffect_      = srcObj.currentEffect_;
}

//--------------------------------------------------------------------------
// Public Member Functions
//--------------------------------------------------------------------------
/* Define static thread member functions (required even after defining them in
 * header file */
struct pt LedCube::ptLayer_;
struct pt LedCube::ptPattern_;

/* ------------------------------------------------------------ */
/* Main run routine and threading functions                     */
/* ------------------------------------------------------------ */
void LedCube::initialize() {
  /* Set databus pins */
  pinMode(OE, OUTPUT);
  pinMode(DATA_0, OUTPUT);
  pinMode(DATA_1, OUTPUT);
  pinMode(DATA_2, OUTPUT);
  pinMode(DATA_3, OUTPUT);
  pinMode(DATA_4, OUTPUT);
  pinMode(DATA_5, OUTPUT);
  pinMode(DATA_6, OUTPUT);
  pinMode(DATA_7, OUTPUT);

  /* Set flip flop demultiplexer pinx */
  pinMode(FF_1, OUTPUT);
  pinMode(FF_2, OUTPUT);
  pinMode(FF_3, OUTPUT);

  /* Set layer demultiplexer pinx */
  pinMode(L_1, OUTPUT);
  pinMode(L_2, OUTPUT);
  pinMode(L_3, OUTPUT);

  /* Initialize data array with zeroes (memset interprets the 
   * data given in parameter 2 as unsigned char) */
  memset(cube_, 0, 64);

  /* Write data to flip flops */
  updateFlipFlops();

  /* Activate flip flop output */
  digitalWrite(OE, LOW);

  /* Set current layer */
  currentLayer_ = 0;

  /* Set parameters */
  refreshRatePattern_ = 250;      /* Update pattern after 250 ms */
  refreshRateLayer_   = 125;        /* Update layer after 1 ms */

  /* Initialize proto threads */
  PT_INIT(&ptLayer_);
  PT_INIT(&ptPattern_);

  /* Choose an effect */
  currentEffect_ = Effect::createEffect(this, String("BoxShrinkGrow"));
  currentEffect_->setParameters(AXIS_X);

  /* Set initialized flag */
  init_ = true;

  /* Enable serial output */
  Serial.begin(9600);
  Serial.println("LED cube initialized");
}

void LedCube::run() {
  updateLayerThread(&ptLayer_, refreshRateLayer_, this);
  updatePatternThread(&ptPattern_, refreshRatePattern_, this);
}

/* This function toggles the LED after 'interval' ms passed */
int LedCube::updateLayerThread(struct pt *pt, int interval, LedCube *instance) {
  static unsigned long timestamp = 0;
  PT_BEGIN(pt);

  while(1) { 
    /* each time the function is called the second boolean argument 
     * "millis() - timestamp > interval" is re-evaluated and if false 
     * the function exits after that. */
    PT_WAIT_UNTIL(pt, millis() - timestamp > interval);
    timestamp = millis(); // take a new timestamp

    /* If more time than refreshRateLayer_ has passed, update layer */
    instance->updateLayer();
  }

  PT_END(pt);
}

int LedCube::updatePatternThread(struct pt *pt, int interval, LedCube *instance) {
  static unsigned long timestamp = 0;
  PT_BEGIN(pt);

  while(1) { 
    /* each time the function is called the second boolean argument 
     * "millis() - timestamp > interval" is re-evaluated and if false 
     * the function exits after that. */
    PT_WAIT_UNTIL(pt, millis() - timestamp > interval);
    timestamp = millis(); // take a new timestamp

    /* If more time than refreshRateLayer_ has passed, update layer */
    instance->updatePattern();
  }

  PT_END(pt);
}

/* ------------------------------------------------------------ */
/* Main update functions for pattern, layer, and flip flops     */
/* ------------------------------------------------------------ */
/* Compute next 512 bit for the cube_ array representing an update in
 * the LED pattern to be displayed. This needs to run at a lower rate 
 * that can actually be perceived by the human eye */
void LedCube::updatePattern() {
  /* Compute new pattern for next time step */
  currentEffect_->update();

  Serial.print("Called updatePattern through pattern thread:"); Serial.println(millis());
}

/* This function runs continuously as fast as possible and is responsible for
 * cycling through all layers such that no flicker appears */
void LedCube::updateLayer() {
  /* Update current layer */
  incrementLayer();

  /* Load data from current layer to flip flops */
  updateFlipFlops();

  /* Activate current layer */
  setLayer();

  Serial.print("Called updateLayer through layer thread: "); Serial.println(millis());
}

/* Update the data in the flip flops, i.e. load data from cube_ array to flip flops. */
void LedCube::updateFlipFlops() {
  /* Update all 8 flip flops of the current layer */
  for (int i = MIN_Y; i <= MAX_Y; i++) {
    updateFlipFlop(i);
  }
}

void LedCube::updateFlipFlop(int i) {
  /* Write data to data bus */
  if(inRangeY(i)) {
    writeData(cube_[currentLayer_][i]);
  } else {
    writeData(0x00);
  }

  /* Flip flops set outputs on a low to high edge */
  setFlipFlop(i);
  setFlipFlop((i+1) % (MAX_Y+1));
}

void LedCube::writeData(unsigned char data){
  digitalWrite(DATA_0, data & 0x01);  /* extract first bit of data character */
  digitalWrite(DATA_1, data & 0x02);
  digitalWrite(DATA_2, data & 0x04);
  digitalWrite(DATA_3, data & 0x08);
  digitalWrite(DATA_0, data & 0x10);  /* extract fifth bit of data character */
  digitalWrite(DATA_1, data & 0x20);
  digitalWrite(DATA_2, data & 0x40);
  digitalWrite(DATA_3, data & 0x80);
}

//--------------------------------------------------------------------------
// Private Member Functions
//--------------------------------------------------------------------------
/* ------------------------------------------------------------ */
/* Control demultiplexers for flip flops and layers             */
/* ------------------------------------------------------------ */
void LedCube::setLayer() {
  setDemultiplexer(currentLayer_, L_1, L_2, L_3);
}

void LedCube::setLayer(int output) {
  setDemultiplexer(output, L_1, L_2, L_3);
}

void LedCube::setFlipFlop(int output) {
  setDemultiplexer(output, FF_1, FF_2, FF_3);
}

void LedCube::setDemultiplexer(int output, int A, int B, int C) {
  if(output == 0) {
    digitalWrite(A, LOW);
    digitalWrite(B, LOW);
    digitalWrite(C, LOW);
  } else if(output == 1) {
    digitalWrite(A, HIGH);
    digitalWrite(B, LOW);
    digitalWrite(C, LOW);
  } else if(output == 2) {
    digitalWrite(A, LOW);
    digitalWrite(B, HIGH);
    digitalWrite(C, LOW);
  } else if(output == 3) {
    digitalWrite(A, HIGH);
    digitalWrite(B, HIGH);
    digitalWrite(C, LOW);
  } else if(output == 4) {
    digitalWrite(A, LOW);
    digitalWrite(B, LOW);
    digitalWrite(C, HIGH);
  } else if(output == 5) {
    digitalWrite(A, HIGH);
    digitalWrite(B, LOW);
    digitalWrite(C, HIGH);
  } else if(output == 6) {
    digitalWrite(A, LOW);
    digitalWrite(B, HIGH);
    digitalWrite(C, HIGH);
  } else if(output == 7) {
    digitalWrite(A, HIGH);
    digitalWrite(B, HIGH);
    digitalWrite(C, HIGH);
  }    
}

void LedCube::incrementLayer() {
  /* Increment layer but keep it in the range of 0 to 7 */
  currentLayer_ = (currentLayer_ + 1) % (MAX_Z + 1);
}

/* ------------------------------------------------------------ */
/* Drawing functions                                            */
/* ------------------------------------------------------------ */
void LedCube::setVoxel(int x, int y, int z) {
  if (inRange(x,y,z)) {
    cube_[z][y] |= (1 << x);
  }
}

void LedCube::clearVoxel(int x, int y, int z) {
  if (inRange(x,y,z)) {
    cube_[z][y] &= ~(1 << x);
  }
}

bool LedCube::getVoxel(int x, int y, int z) {
  if (inRange(x,y,z)) {
    if (cube_[z][y] & (1 << x)) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

void LedCube::alterVoxel(int x, int y, int z, bool state) {
  if (state == true) {
    setVoxel(x,y,z);
  } else {
    clearVoxel(x,y,z);
  }
}

void LedCube::flipVoxel(int x, int y, int z) {
  if (inRange(x, y, z)) {
    cube_[z][y] ^= (1 << x);
  }
}

void LedCube::fill(unsigned char pattern) {
  for (int z = MIN_Z; z <= MAX_Z; z++) {
    for (int y = MIN_Y; y <= MAX_Y; y++) {
      cube_[z][y] = pattern;
    }
  }
}

/* ------------------------------------------------------------ */
/* Box drawing functions                                        */
/* ------------------------------------------------------------ */
void LedCube::boxFilled(int x1, int y1, int z1, int x2, int y2, int z2) {
  argOrder(x1, x2, &x1, &x2);
  argOrder(y1, y2, &y1, &y2);
  argOrder(z1, z2, &z1, &z2);

  for (int iz = z1; iz <= z2; iz++) {
    for (int iy = y1; iy <= y2; iy++) {
      cube_[iz][iy] |= byteLine(x1,x2);
    }
  }
}

void LedCube::boxWalls(int x1, int y1, int z1, int x2, int y2, int z2) {
  argOrder(x1, x2, &x1, &x2);
  argOrder(y1, y2, &y1, &y2);
  argOrder(z1, z2, &z1, &z2);

  for (int iz = z1; iz <= z2; iz++) {
    for (int iy = y1; iy <= y2; iy++) { 
      if (iy == y1 || iy == y2 || iz == z1 || iz == z2) {
        cube_[iz][iy] = byteLine(x1,x2);
      } else {
        cube_[iz][iy] |= ((0x01 << x1) | (0x01 << x2));
      }
    }
  }
}

void LedCube::boxWireframe(int x1, int y1, int z1, int x2, int y2, int z2) {
  argOrder(x1, x2, &x1, &x2);
  argOrder(y1, y2, &y1, &y2);
  argOrder(z1, z2, &z1, &z2);

  /* Lines along X axis */
  cube_[z1][y1] = byteLine(x1,x2);
  cube_[z1][y2] = byteLine(x1,x2);
  cube_[z2][y1] = byteLine(x1,x2);
  cube_[z2][y2] = byteLine(x1,x2);

  /* Lines along Y axis */
  for (int iy = y1; iy <= y2; iy++) {
    setVoxel(x1,iy,z1);
    setVoxel(x1,iy,z2);
    setVoxel(x2,iy,z1);
    setVoxel(x2,iy,z2);
  }

  /* Lines along Z axis */
  for (int iz = z1; iz <= z2; iz++) {
    setVoxel(x1,y1,iz);
    setVoxel(x1,y2,iz);
    setVoxel(x2,y1,iz);
    setVoxel(x2,y2,iz);
  }
}

/* ------------------------------------------------------------ */
/* Line functions                                               */
/* ------------------------------------------------------------ */
void LedCube::line(int x1, int y1, int z1, int x2, int y2, int z2) {
  float ky, kz;
  unsigned char x,y,z;

  /* If x1 is bigget than x2, we need to flip all the values. */
  if (x1 > x2) {
    int tmp;
    tmp = x2; x2 = x1; x1 = tmp;
    tmp = y2; y2 = y1; y1 = tmp;
    tmp = z2; z2 = z1; z1 = tmp;
  }

  if(x1 == x2) {
    /* This case is required, since ky and kz would be infinite o.w. */
    // TODO: fix case where x1 = x2

  } else {
    if (y1 > y2) {
      ky = (float)(y1 - y2) / (float)(x2 - x1);
    } else {
      ky = (float)(y2 - y1) / (float)(x2 - x1);
    }

    if (z1 > z2) {
      kz = (float)(z1 - z2) / (float)(x2 - x1);
    } else {
      kz = (float)(z2 - z1) / (float)(x2 - x1);
    }

    for (x = x1; x <= x2; x++) {
      y = (ky * (x - x1)) + y1;
      z = (kz * (x - x1)) + z1;
      setVoxel(x, y, z);
    }
  }
}

/* ------------------------------------------------------------ */
/* Mirror functions                                             */
/* ------------------------------------------------------------ */
void LedCube::mirrorX() {
  unsigned char buffer[8][8];

  memcpy(buffer, cube_, 64); // copy the current cube into a buffer.

  fill(0x00);

  for (int z = MIN_Z; z <= MAX_Z; z++) {
    for (int y = MIN_Y; y < MAX_Y; y++) {
      cube_[z][y] = flipByte(buffer[z][y]);
    }
  }
}

void LedCube::mirrorY() {
  unsigned char buffer[8][8];

  memcpy(buffer, cube_, 64); // copy the current cube into a buffer.

  fill(0x00);
  for (int z = MIN_Z; z <= MAX_Z; z++) {
    for (int y = MIN_Y; y <= MAX_Y; y++) {
      for (int x = MIN_X; x <= MAX_X; x++) {
        if (buffer[z][y] & (0x01 << x)) {
          setVoxel(x,MAX_Y-y,z);
        }
      }
    }
  }
}

void LedCube::mirrorZ() {
  unsigned char buffer[8][8];

  memcpy(buffer, cube_, 64); // copy the current cube into a buffer.

  for (int y = MIN_Y; y <= MAX_Y; y++) {
    for (int z = MIN_Z; z <= MAX_Z; z++) {
      cube_[MAX_Z - z][y] = buffer[z][y];
    }
  }
}

/* ------------------------------------------------------------ */
/* Shfit functions                                              */
/* ------------------------------------------------------------ */
void LedCube::shift(int axis, bool direction) {
  /* direction can be two values
   * 0 ... shift towards lower coordinate values along chosen axis
   *       (the values stored in the lowest layer are shifted out, i.e.
   *       their content will be lost)
   * 1 ... shift towards higher coordinate values along chosen axis
   *       (the values stored in the highest layer are shifted out, i.e.
   *       their content will be lost)
   */
  if(axis == AXIS_X) {
    shiftX(direction);
  } else if (axis == AXIS_Y) {
    shiftY(direction);
  } else {
    shiftZ(direction);
  }
}

void LedCube::shiftX(bool direction) {
  if(direction == false) {
    /* Copy planes to lower layers */
    for(int x = MIN_X + 1; x <= MAX_X; x++) {
      copyPlaneX(x, x - 1);
    }

    /* Reset highest layer to zeros */
    setPlaneX(MAX_X, false);
  } else {
    /* Copy planes to higher layers */
    for(int x = MIN_X; x < MAX_X; x++) {
      copyPlaneX(x, x + 1);
    }

    /* Reset lowest layer to zeros */
    setPlaneX(MIN_X, false);
  }
}

void LedCube::shiftY(bool direction) {
  if(direction == false) {
    /* Copy planes to lower layers */
    for(int y = MIN_Y + 1; y <= MAX_Y; y++) {
      copyPlaneY(y, y - 1);
    }

    /* Reset highest layer to zeros */
    setPlaneY(MAX_Y, false);
  } else {
    /* Copy planes to higher layers */
    for(int y = MIN_Y; y < MAX_Y; y++) {
      copyPlaneY(y, y + 1);
    }

    /* Reset lowest layer to zeros */
    setPlaneY(MIN_Y, false);
  }
}

void LedCube::shiftZ(bool direction) {
  if(direction == false) {
    /* Copy planes to lower layers */
    for(int z = MIN_Z + 1; z <= MAX_Z; z++) {
      copyPlaneZ(z, z - 1);
    }

    /* Reset highest layer to zeros */
    setPlaneZ(MAX_Z, false);
  } else {
    /* Copy planes to higher layers */
    for(int z = MIN_Z; z < MAX_Z; z++) {
      copyPlaneZ(z, z + 1);
    }

    /* Reset lowest layer to zeros */
    setPlaneZ(MIN_Z, false);
  }
}

/* ------------------------------------------------------------ */
/* Set and clear planes functions                               */
/* ------------------------------------------------------------ */
void LedCube::setPlane(int i, int plane, bool data) {
  if(plane == AXIS_X) {
    setPlaneX(i, data);
  } else if (plane == AXIS_Y) {
    setPlaneY(i, data);
  } else {
    setPlaneZ(i, data);
  }
}

void LedCube::setPlaneX(int x, bool data) {
  if (inRangeX(x)) {
    for (int z = MIN_Z; z <= MAX_Z; z++) {
      for (int y = MIN_Y; y <= MAX_Y; y++) {
        cube_[z][y] |= (data << x);
      }
    }
  }
}

void LedCube::setPlaneY(int y, bool data) {
  unsigned char tmp;

  if(data) {
    tmp = 0xff;
  } else {
    tmp = 0x00;
  }

  /* If no data is handed to the function, it clears the plane, i.e. 
   * sets cube_[i][y] to 0x00 */
  if (inRangeY(y)) {
    for (int i = MIN_Z; i <= MAX_Z; i++) {
      cube_[i][y] = tmp;
    }
  }
}

void LedCube::setPlaneZ(int z, bool data) {
  unsigned char tmp;

  if(data) {
    tmp = 0xff;
  } else {
    tmp = 0x00;
  }

  /* If no data is handed to the function, it clears the plane, i.e. 
   * sets cube_[z] to 0x00 */
  if (inRangeZ(z)) {
    for (int i = MIN_Y; i < MAX_Y; i++) {
      cube_[z][i] = tmp;
    }
  }
}

void LedCube::copyPlane(int src, int dst, int plane) {
  if(plane == AXIS_X) {
    copyPlaneX(src, dst);
  } else if (plane == AXIS_Y) {
    copyPlaneY(src, dst);
  } else {
    copyPlaneZ(src, dst);
  }
}

void LedCube::copyPlaneX(int src, int dst) {
  for(int z = MIN_Z; z <= MAX_Z; z++) {
    for(int y = MIN_Y; y <= MAX_Y; y++) {
      alterVoxel(dst, y, z, getVoxel(src, y, z));
    }
  }
}

void LedCube::copyPlaneY(int src, int dst) {
  for(int z = MIN_Z; z <= MAX_Z; z++) {
    cube_[z][dst] = cube_[z][src];
  }
}

void LedCube::copyPlaneZ(int src, int dst) {
  for(int y = MIN_Y; y <= MAX_Y; y++) {
    cube_[dst][y] = cube_[src][y];
  }
}

/* ------------------------------------------------------------ */
/* These functions validate that we are drawing inside the cube */
/* ------------------------------------------------------------ */
bool LedCube::inRange(int x, int y, int z) {
  if (x >= MIN_X && x <= MAX_X && y >= MIN_Y && y <= MAX_Y && z >= MIN_Z && z <= MAX_Z) {
    return true;
  } else {
    return false;
  }
}

bool LedCube::inRangeX(int x) {
  if (x >= MIN_X && x <= MAX_X) {
    return true;
  } else {
    return false;
  }
}

bool LedCube::inRangeY(int y) {
  if (y >= MIN_Y && y <= MAX_Y) {
    return true;
  } else {
    return false;
  }
}

bool LedCube::inRangeZ(int z) {
  if (z >= MIN_Z && z <= MAX_Z) {
    return true;
  } else {
    return false;
  }
}

/* ------------------------------------------------------------ */
/* Utility functions                                            */
/* ------------------------------------------------------------ */
char LedCube::byteLine(int start, int end) {
  /* Returns a byte with a row of 1's drawn in it.
     e.g. byteline(2,5) gives 0b00111100 */
  return ((0xff<<start) & ~(0xff<<(end+1)));
}

void LedCube::argOrder(int ix1, int ix2, int *ox1, int *ox2) {
  /* Makes sure x1 is alwas smaller than x2 */
  if (ix1>ix2) {
    int tmp;
    tmp = ix1;
    ix1= ix2;
    ix2 = tmp;
  }

  *ox1 = ix1;
  *ox2 = ix2;
}

char LedCube::flipByte (char byte) {
  char flop = 0x00;

  flop = (flop & 0b11111110) | (0b00000001 & (byte >> 7));
  flop = (flop & 0b11111101) | (0b00000010 & (byte >> 5));
  flop = (flop & 0b11111011) | (0b00000100 & (byte >> 3));
  flop = (flop & 0b11110111) | (0b00001000 & (byte >> 1));
  flop = (flop & 0b11101111) | (0b00010000 & (byte << 1));
  flop = (flop & 0b11011111) | (0b00100000 & (byte << 3));
  flop = (flop & 0b10111111) | (0b01000000 & (byte << 5));
  flop = (flop & 0b01111111) | (0b10000000 & (byte << 7));

  return flop;
}

/* ------------------------------------------------------------ */
/* Get functions                                                */
/* ------------------------------------------------------------ */
int LedCube::getRefreshRateLayer() {
  return refreshRateLayer_;
}

int LedCube::getRefreshRatePattern() {
  return refreshRatePattern_;
}

Effect* LedCube::getCurrentEffect() {
  return currentEffect_;
}

/* ------------------------------------------------------------ */
/* Set functions                                                */
/* ------------------------------------------------------------ */
void LedCube::setRefreshRateLayer(int refreshRate) {
  refreshRateLayer_ = refreshRate;
}

void LedCube::setRefreshRatePattern(int refreshRate) {
  refreshRatePattern_ = refreshRate;
}

void LedCube::setEffect(String name) {
  currentEffect_ = Effect::createEffect(this, name);
}
