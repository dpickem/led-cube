/* -------------------------------------------------------------------------------
 Effect.cpp

 This class implements a factory to create instances of the various effect classes
 that are derived from the abstract base Effect class 
 
 NOTES:
 
 EXAMPLES:

 Initially created Daniel Pickem on 14/12/2013
 ------------------------------------------------------------------------------- */

//------------------------------------------------------------------------------
// Includes
//------------------------------------------------------------------------------
#include "Effect.h"

//------------------------------------------------------------------------------
// Base Effect Class
//------------------------------------------------------------------------------
/* Constructors */
Effect::Effect() {

}

/* Destructor */
Effect::~Effect() {

}

/* Virtual constructor of Effect class, the class to be instantiated does not 
 * have to be known at compile time --> this is the factory design pattern */
Effect *Effect::createEffect(LedCube *instance, String choice) {
  /* Instantiate effect */
  if(choice.equals("PlaneBoing")) {
      return new EffectPlaneBoing(instance);
  } else if(choice.equals("BoxShrinkGrow")) {
      return new EffectBoxShrinkGrow(instance);
  } else if(choice.equals("Rain")) {
      return new EffectRain(instance);
  } else {
      return new EffectPlaneBoing(instance);
  }
}

//------------------------------------------------------------------------------
// Effect PlaneBoing Class
//------------------------------------------------------------------------------
EffectPlaneBoing::EffectPlaneBoing(LedCube *instance) {
  /* Store pointer to LedCube instance */
  cube_ = instance;

  /* Initialize parameters */
  axis_         = int(AXIS_X);
  direction_    = FORWARD; 
  currentLayer_ = 0;
}

void EffectPlaneBoing::update() {
  /* compute the next layer index */
  updateLayer();

  /* Set current layer */
  cube_->setPlane(currentLayer_, axis_, true);

  /* Unset all other layers */
  for (int i = MIN_X; i <= MAX_X; i++) {
    if(i != currentLayer_) {
      cube_->setPlane(i, axis_, false);
    }
  }

  Serial.print("Effect PlaneBoing updated: "); Serial.println(millis());
}

void EffectPlaneBoing::updateLayer() {
  // TODO: this function assumes MAX_X = MAX_Y = MAX_Z
  /* Update currentLayer and direction if necessary */
  if(direction_ == FORWARD) {
    if(currentLayer_ + 1 <= MAX_X) {
      /* Keep going in the current direction */
      currentLayer_++;
    } else {
      /* Reverse direction */
      currentLayer_--;
      direction_ = BACKWARD;
    }
  } else {
    if(currentLayer_ - 1 >= MIN_X) {
      /* Keep going in the current direction */
      currentLayer_--;
    } else {
      /* Reverse direction */
      currentLayer_++;
      direction_ = FORWARD;
    }
  }
}

void EffectPlaneBoing::setParameters(Parameters params) {
  axis_ = params.getParam(0);
}

//------------------------------------------------------------------------------
// Effect BoxShrinkGrow Class
//------------------------------------------------------------------------------
EffectBoxShrinkGrow::EffectBoxShrinkGrow(LedCube *instance) {
  /* Store pointer to LedCube instance */
  cube_ = instance;

  /* Initialize default parameters */
  mode_             = 0;
  direction_        = 0; 
  grow_             = true;
  currentIteration_ = 0;
}

void EffectBoxShrinkGrow::update() {
  int x1 = 0;
  int y1 = 0;
  int z1 = 0;
  int x2 = 0;
  int y2 = 0;
  int z2 = 0;

  /* Update iteration, i.e. currentIteration_ variable and grow flag */
  updateIteration();

  switch(direction_) {
    case(0):
      x1 = 0; x2 = currentIteration_; 
      y1 = 0; y2 = currentIteration_;
      z1 = 0; z2 = currentIteration_;
      break;
    case(1):    // TODO: supposedly fails
      x1 = 0;                         x2 = currentIteration_; 
      y1 = 0;                         y2 = currentIteration_;
      z1 = MAX_Z - currentIteration_; z2 = MAX_Z;
      break;
    case(2):
      x1 = 0;       x2 = currentIteration_; 
      y1 = MAX_Y;   y2 = MAX_Y - currentIteration_;
      z1 = 0;       z2 = currentIteration_;
      break;
    case(3):    // TODO: supposedly fails
      x1 = 0;       x2 = currentIteration_; 
      y1 = MAX_Y;   y2 = MAX_Y - currentIteration_;
      z1 = MAX_Z;   z2 = MAX_Z - currentIteration_;
      break;
    case(4):    // TODO: supposedly fails
      x1 = MAX_X;   x2 = MAX_X - currentIteration_; 
      y1 = 0;       y2 = currentIteration_;
      z1 = 0;       z2 = currentIteration_;
      break;
    case(5):
      x1 = MAX_X;   x2 = MAX_X - currentIteration_; 
      y1 = 0;       y2 = currentIteration_;
      z1 = MAX_Z;   z2 = MAX_Z - currentIteration_;
      break;
    case(6):    // TODO: supposedly fails
      x1 = MAX_X;   x2 = MAX_X - currentIteration_; 
      y1 = MAX_Y;   y2 = MAX_Y - currentIteration_;
      z1 = 0;       z2 = currentIteration_;
      break;
    case(7):
      x1 = MAX_X;   x2 = MAX_X - currentIteration_; 
      y1 = MAX_Y;   y2 = MAX_Y - currentIteration_;
      z1 = MAX_Z;   z2 = MAX_Z - currentIteration_;
      break;
    default:
      /* Make default case the same as case 0 */
      x1 = 0; x2 = currentIteration_; 
      y1 = 0; y2 = currentIteration_;
      z1 = 0; z2 = currentIteration_;
      break;
  }

  /* Remove previously drawn box */
  cube_->fill(0x00);

  /* Draw box based on mode parameter */
  if (mode_ == 0) {
    cube_->boxWireframe(x1,y1,z1,x2,y2,z2);
  } else if (mode_ == 1) {
    cube_->boxWalls(x1,y1,z1,x2,y2,z2);
  } else {
    cube_->boxFilled(x1,y1,z1,x2,y2,z2);
  }
}

void EffectBoxShrinkGrow::updateIteration() {
  if(grow_) {
    if(currentIteration_ + 1 <= MAX_X) {
      currentIteration_++;
    } else {
      grow_ = false;
      currentIteration_--;
    }
  } else {
    if(currentIteration_ - 1 >= MIN_X) {
      currentIteration_--;
    } else {
      grow_ = true;
      currentIteration_++;
    }
  }
}

void EffectBoxShrinkGrow::setParameters(Parameters params) {
  mode_       = int(params.getParam(0));
  direction_  = int(params.getParam(1));
}

//------------------------------------------------------------------------------
// Effect BoxWoopWoop Class
//------------------------------------------------------------------------------
EffectBoxWoopWoop::EffectBoxWoopWoop(LedCube *instance) {
  /* Store pointer to LedCube instance */
  cube_ = instance;

  /* Initialize default parameters */
  grow_             = true;
  currentIteration_ = 0;
}

void EffectBoxWoopWoop::update() {
  /* Clear cube */
  cube_->fill(0x00);

  /* Draw wireframe box with current iteration determining the size */
  // TODO: parameterize this with MIN_i and MAX_i
  int i = currentIteration_;
  cube_->boxWireframe(3 - i, 3 - i, 3 - i, 4 + i, 4 + i, 4 + i);

  /* Update currentIteration */
  updateIteration();
}

void EffectBoxWoopWoop::updateIteration() {
  // TODO: parameterize this with MIN_i and MAX_i
  if(grow_ == true) {
    if(currentIteration_ + 1 < 3) {
      currentIteration_++;
    } else {
      currentIteration_--;
      grow_ = false;
    }
  } else {
    if(currentIteration_ - 1 > 0) {
      currentIteration_--;
    } else {
      currentIteration_++;
      grow_ = true;
    }
  }
}

void EffectBoxWoopWoop::setParameters(Parameters params) {
  /* No parameters need to be set for this effect */
}

//------------------------------------------------------------------------------
// Effect Rain Class
//------------------------------------------------------------------------------
EffectRain::EffectRain(LedCube *instance) {
  /* Store pointer to LedCube instance */
  cube_ = instance;

  /* Set parameters */
  numberOfDrops_ = 4;
}

void EffectRain::update() {
  /* Shift all layers down such that the top layer is empty */
  cube_->shift(AXIS_Z, 0);

  /* Choose a random number of rain drops */
  int rnd_num = rand() % numberOfDrops_;

  /* Set random number of raindrops in upper layer */
  for (int i = 0; i < rnd_num; i++) {
    cube_->setVoxel(rand() % (MAX_X + 1), rand() % (MAX_Y + 1), MAX_Z);
  }
}

void EffectRain::setParameters(Parameters params) {
  numberOfDrops_ = int(params.getParam(0));
}
