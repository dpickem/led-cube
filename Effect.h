/* -------------------------------------------------------------------------------
 Effect.h

 This class implements a factory to create instances of the various effect classes
 that are derived from the abstract base Effect class 
 
 NOTES:
 - Virtual constructors are used for creating  a copy of an object or a new object 
   without knowing its concrete type.

   See http://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Virtual_Constructor
 - Factory design pattern 
   http://sourcemaking.com/design_patterns/factory_method/cpp/1
 
 EXAMPLES:

 Initially created Daniel Pickem on 14/12/2013

 This library is in part based on the library available at 
    http://www.instructables.com/id/Led-Cube-8x8x8/?ALLSTEPS
 ------------------------------------------------------------------------------- */

#ifndef _EFFECT_h_
#define _EFFECT_h_

//------------------------------------------------------------------------------
// Effect related definitions 
//------------------------------------------------------------------------------
#define X 0
#define Y 1
#define Z 2

#define AXIS_X 0
#define AXIS_Y 1
#define AXIS_Z 2

#define FORWARD   0
#define BACKWARD  1

//------------------------------------------------------------------------------
// Includes
//------------------------------------------------------------------------------
#include <Arduino.h>
#include <math.h>
#include "LedCube.h"

/* Forward declaration of LedCube class, required for having a pointer to an
 * instance of LedCube in class Effect 
 * See:
 * http://stackoverflow.com/questions/8526819/c-header-files-including-each-other-mutually
 */
class LedCube;

class Parameters {
  public: 
    Parameters(float p0 = 0.0, float p1 = 0.0, float p2 = 0.0, float p3 = 0.0, float p4 = 0.0) {
      params_[0] = p0;
      params_[1] = p1;
      params_[2] = p2;
      params_[3] = p3;
      params_[4] = p4;
    };

    ~Parameters() {};

    float getParam(int i) { 
      if(i >= 0 && i <= 4) {
        return params_[i]; 
      }
    }

  private:
    float params_[5];
};

class Effect {
  public:
    /* Default constructor, not used since this is an abstract class */
    Effect();

    /* Destructor */
    virtual ~Effect();

    /* Factory Method, i.e. virtual constructor */
    static Effect *createEffect(LedCube *instance, String choice);

    //--------------------------------------------------------------------------
    // Public Member Functions
    //--------------------------------------------------------------------------
    /* Virtual methods to be implemented by all derived classes */
    virtual void update() = 0;
    virtual void setParameters(Parameters params) = 0;

    //--------------------------------------------------------------------------
    // Public Member Variables 
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Note: Private member variables and functions dont make sense for an
    // abstract class.
    //--------------------------------------------------------------------------
};

class EffectPlaneBoing: public Effect {
  public: 
    EffectPlaneBoing(LedCube *instance);
    void update();
    void updateLayer();
    void setParameters(Parameters params);

  private:
    int axis_;
    int direction_;
    int currentLayer_;
    LedCube *cube_;
};

class EffectBoxShrinkGrow: public Effect {
  public: 
    EffectBoxShrinkGrow(LedCube *instance);
    void update();
    void updateIteration();
    void setParameters(Parameters params);

  private:
    int mode_;
    int direction_;
    bool grow_;
    int currentIteration_;
    LedCube *cube_;
};

class EffectBoxWoopWoop: public Effect {
  public: 
    EffectBoxWoopWoop(LedCube *instance);
    void update();
    void updateIteration();
    void setParameters(Parameters params);

  private:
    bool grow_;
    int currentIteration_;
    LedCube *cube_;
};

class EffectRain: public Effect {
  public: 
    EffectRain(LedCube *instance);
    void update();
    void setParameters(Parameters params);

  private:
    int numberOfDrops_;
    LedCube *cube_;
};
#endif
